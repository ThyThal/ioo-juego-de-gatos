﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Game;

namespace UnitModule
{
    [TestClass]
    public class BulletTestModule
    {
        [TestMethod]
        public void BulletConstructor()
        {
            // Mock Player Constructor Data.
            List<GameObject> listBullets = new List<GameObject>();
            List<GameObject> listPlayer = new List<GameObject>();
            List<GameObject> listEnemy = new List<GameObject>();

            Vector2 position = Vector2.One;
            Vector2 scale = Vector2.One;
            float angle = 10f;

            // Crear Prueba.
            var bullet = new Bullet(position, scale, angle, listBullets, listPlayer, listEnemy);

            // Confirmar Datos.
            Assert.IsNotNull(bullet);

            Assert.AreEqual(position, bullet.render.transform.Position);
            Assert.AreEqual(scale, bullet.render.transform.Scale);
            Assert.AreEqual(angle, bullet.render.transform.Angle);
        }

        [TestMethod]
        public void BulletResetTransform()
        {
            // Mock Player Constructor Data.
            List<GameObject> listBullets = new List<GameObject>();
            List<GameObject> listPlayer = new List<GameObject>();
            List<GameObject> listEnemy = new List<GameObject>();

            Vector2 position = Vector2.One;
            Vector2 scale = Vector2.One;
            float angle = 10f;

            Vector2 newPosition = Vector2.Zero;
            Vector2 newScale = Vector2.Zero;
            float newAngle = 0f;

            // Crear Prueba.
            var bullet = new Bullet(position, scale, angle, listBullets, listPlayer, listEnemy);
            bullet.ResetTransform(Vector2.Zero, Vector2.Zero, 0f);

            // Confirmar Datos.
            Assert.IsNotNull(bullet);

            Assert.AreEqual(newPosition, bullet.render.transform.Position);
            Assert.AreEqual(newScale, bullet.render.transform.Scale);
            Assert.AreEqual(newAngle, bullet.render.transform.Angle);
        }

        [TestMethod]
        public void BulletGiveValues()
        {
            // Manager Bullet List
            List<GameObject> listBullets = new List<GameObject>();
            List<GameObject> listPlayer = new List<GameObject>();
            List<GameObject> listEnemy = new List<GameObject>();

            // Mock Player Constructor Data.
            Vector2 position = Vector2.One;
            Vector2 scale = Vector2.One;
            float angle = 10f;

            var type = Bullet.Type.Player;
            float speed = 100f;
            int damage = 50;
            bool aimbot = true;

            // Crear Prueba.
            var bullet = new Bullet(position, scale, angle, listBullets, listPlayer, listEnemy);
            bullet.GiveValues(type, speed, damage, aimbot); // No encuentra la lista de Bullets del LevelController.

            // Confirmar Datos.
            Assert.IsNotNull(listBullets);
            Assert.IsNotNull(bullet);
            Assert.AreEqual(damage, bullet.Damage);
        }

        [TestMethod]
        public void BulletCheckCollisions()
        {
            // Mock Data.
            List<GameObject> listBullets = new List<GameObject>();
            List<GameObject> listPlayer = new List<GameObject>();
            List<GameObject> listEnemy = new List<GameObject>();

            Vector2 position = Vector2.One;
            Vector2 scale = Vector2.One;
            float angle = 10f;
            float speed = 20f;
            int maxLife = 100;

            // Crear Prueba
            var player = new Player(position, scale, angle, speed, maxLife);
            listPlayer.Add(player);
            var bullet = new Bullet(position, scale, angle, listBullets, listPlayer, listEnemy);
            bullet.CheckCollisions(listPlayer);

            // Confirmar Datos.
            Assert.IsNotNull(bullet);
            Assert.IsNotNull(player);
            Assert.IsNotNull(listPlayer);

            Assert.AreEqual(listPlayer[0], player as GameObject);
            Assert.AreEqual(position, bullet.render.transform.Position);
            Assert.AreEqual(scale, bullet.render.transform.Scale);
            Assert.AreEqual(angle, bullet.render.transform.Angle);
        }

        [TestMethod]
        public void BulletAdd()
        {
            // Mock Data.
            List<GameObject> listBullets = new List<GameObject>();
            List<GameObject> listPlayer = new List<GameObject>();
            List<GameObject> listEnemy = new List<GameObject>();

            Vector2 position = Vector2.One;
            Vector2 scale = Vector2.One;
            float angle = 10f;

            // Crear Prueba
            var bullet = new Bullet(position, scale, angle, listBullets, listPlayer, listEnemy);
            bullet.AddBullet(listBullets);

            // Confirmar Datos.
            Assert.IsNotNull(bullet);
            Assert.IsNotNull(listBullets);

            Assert.AreEqual(listBullets.Count, 1);
        }

        [TestMethod]
        public void BulletRemove()
        {
            // Mock Data.
            List<GameObject> listBullets = new List<GameObject>();
            List<GameObject> listPlayer = new List<GameObject>();
            List<GameObject> listEnemy = new List<GameObject>();

            Vector2 position = Vector2.One;
            Vector2 scale = Vector2.One;
            float angle = 10f;

            // Crear Prueba
            var bullet = new Bullet(position, scale, angle, listBullets, listPlayer, listEnemy);
            bullet.AddBullet(listBullets);
            bullet.RemoveBullet(listBullets);


            // Confirmar Datos.
            Assert.IsNotNull(bullet);
            Assert.IsNotNull(listBullets);

            Assert.AreEqual(listBullets.Count, 0);
        }
    }
}