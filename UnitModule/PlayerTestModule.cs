﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Game;

namespace UnitModule
{
    [TestClass]
    public class PlayerTestModule
    {
        [TestMethod]
        public void PlayerConstructor()
        {
            // Mock Player Constructor Data.
            Vector2 position = Vector2.One;
            Vector2 scale = Vector2.One;
            float angle = 10f;
            float speed = 20f;
            int maxLife = 100;

            // Crear Prueba.
            var player = new Player(position, scale, angle, speed, maxLife);

            // Confirmar Datos.
            Assert.IsNotNull(player);

            Assert.AreEqual(position, player.render.transform.Position);
            Assert.AreEqual(scale, player.render.transform.Scale);
            Assert.AreEqual(angle, player.render.transform.Angle);

        }

        [TestMethod]
        public void PlayerBulletCreator()
        {
            
        }
    }
}
