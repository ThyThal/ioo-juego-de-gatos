﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class MainMenu
    {
        Render render;
        private MainMenu(Render render)
        {
            this.render = render;
        }
        
        private const float INPUT_DELAY_TIME = 0.2f;
        private float currentInputDelay;

        private const string MAIN_MENU_PATH = "Textures/Menus/MainMenu.png";

        private List<String> PATH_NORMAL_CREDITS = new List<string>();
        private List<String> PATH_NORMAL_PLAY = new List<string>();
        private List<String> PATH_HIGH_PLAY = new List<string>();

        private List<String> PATH_HIGH_CREDITS = new List<string>();
        private List<String> PATH_NORMAL_EXIT = new List<string>();
        private List<String> PATH_HIGH_EXIT = new List<string>();

        private List<Button> buttons = new List<Button>();


        private Button playButton;
        private Button creditsButton;
        private Button quitButton;
        private Button currentButton;

        public MainMenu()
        {
            CreatePaths();

            playButton = new Button(PATH_NORMAL_PLAY, PATH_HIGH_PLAY, new Vector2(250, 225), new Vector2(1, 1), 0);
            creditsButton = new Button(PATH_NORMAL_CREDITS, PATH_HIGH_CREDITS, new Vector2(250, 350), new Vector2(1, 1), 0);
            quitButton = new Button(PATH_NORMAL_EXIT, PATH_HIGH_EXIT, new Vector2(250, 475), new Vector2(1, 1), 0);

            buttons.Add(playButton);
            buttons.Add(creditsButton);
            buttons.Add(quitButton);

            playButton.AssignButton(quitButton, creditsButton);
            creditsButton.AssignButton(playButton, quitButton);
            quitButton.AssignButton(creditsButton, playButton);

            currentButton = playButton;
            currentButton.HighlightButton();
        }

        public void Update()
        {
            MenuInput();
        }

        public void Renderer()
        {
            Engine.Draw(MAIN_MENU_PATH, 0, 0);
            foreach (var button in buttons)
            {
                button.render.Renderer();
            }
        }
        private void MenuInput()
        {
            currentInputDelay -= Program.deltaTime;

            if (Engine.GetKey(Keys.UP) && currentInputDelay <= 0)
            {
                ChangeButton(currentButton.PreviousButton);
            }

            if (Engine.GetKey(Keys.DOWN) && currentInputDelay <= 0)
            {
                ChangeButton(currentButton.NextButton);
            }

            if (Engine.GetKey(Keys.SPACE) && currentInputDelay <= 0)
            {
                SelectButton(currentButton);
            }
        }
        private void SelectButton(Button selectedButton)
        {
            if (selectedButton == playButton)
            {
                GameManager.Instance.ChangeGameState(GameState.Level);
            }

            else if (selectedButton == creditsButton)
            {
                GameManager.Instance.ChangeGameState(GameState.Credits);
            }


            else if (selectedButton == quitButton)
            {
                GameManager.Instance.ExitGame();
            }
        }
        private void ChangeButton(Button newButton)
        {
            currentInputDelay = INPUT_DELAY_TIME;
            currentButton.UnhighlightButton();
            currentButton = newButton;
            currentButton.HighlightButton();
        }
        private void CreatePaths()
        {
            // Normal Textures
            #region
            int f1 = Directory.GetFiles("Textures/Buttons/Normal/Play/", "*", SearchOption.AllDirectories).Length;
            for (int i = 0; i < f1; i++)
            {
                PATH_NORMAL_PLAY.Add($"Textures/Buttons/Normal/Play/{i}.png");
            }

            int f2 = Directory.GetFiles("Textures/Buttons/Normal/Credits/", "*", SearchOption.AllDirectories).Length;
            for (int i = 0; i < f2; i++)
            {
                PATH_NORMAL_CREDITS.Add($"Textures/Buttons/Normal/Credits/{i}.png");
            }

            int f3 = Directory.GetFiles("Textures/Buttons/Normal/Exit/", "*", SearchOption.AllDirectories).Length;
            for (int i = 0; i < f3; i++)
            {
                PATH_NORMAL_EXIT.Add($"Textures/Buttons/Normal/Exit/{i}.png");
            }
            #endregion

            // Highlighted Textures
            #region
            int f4 = Directory.GetFiles("Textures/Buttons/High/Play/", "*", SearchOption.AllDirectories).Length;
            for (int i = 0; i < f4; i++)
            {
                PATH_HIGH_PLAY.Add($"Textures/Buttons/High/Play/{i}.png");
            }

            int f5 = Directory.GetFiles("Textures/Buttons/High/Credits/", "*", SearchOption.AllDirectories).Length;
            for (int i = 0; i < f5; i++)
            {
                PATH_HIGH_CREDITS.Add($"Textures/Buttons/High/Credits/{i}.png");
            }

            int f6 = Directory.GetFiles("Textures/Buttons/High/Exit/", "*", SearchOption.AllDirectories).Length;
            for (int i = 0; i < f6; i++)
            {
                PATH_HIGH_EXIT.Add($"Textures/Buttons/High/Exit/{i}.png");
            }
            #endregion
        }
    }
}
