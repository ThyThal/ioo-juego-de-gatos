﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class CreditsScreen
    {
        private string texturePath;

        public CreditsScreen(string texturePath)
        {
            this.texturePath = texturePath;
        }

        public void Update()
        {
            if (Engine.GetKey(Keys.ESCAPE))
            {
                GameManager.Instance.ChangeGameState(GameState.MainMenu);
            }
        }

        public void Render()
        {
            Engine.Draw(texturePath);
        }
    }
}
