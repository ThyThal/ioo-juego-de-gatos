﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class Button : GameObject
    {
        private Animation normalAnimation;
        private Animation highlightedAnimation;
        private List<string> texturePath;
        private List<string> highlightPath;
        public Button NextButton { get; private set; }
        public Button PreviousButton { get; private set; }

        public Button(List<string> texturePath, List<string> highlightPath, Vector2 position, Vector2 scale, float angle) : base(position, scale, angle)
        {
            this.texturePath = texturePath;
            this.highlightPath = highlightPath;
            CreateAnimations();
            UnhighlightButton();
        }

        public void AssignButton(Button previousButton, Button nextButton)
        {
            NextButton = nextButton;
            PreviousButton = previousButton;
        }

        public void HighlightButton()
        {
            render.currentAnimation = highlightedAnimation;
        }

        public void UnhighlightButton()
        {
            render.currentAnimation = normalAnimation;
        }

        protected override void CreateAnimations()
        {
            // List with Normal animation.
            List<Texture> normalTextures = new List<Texture>();
            for (int i = 0; i < texturePath.Count; i++)
            {
                Texture frame = Engine.GetTexture(texturePath[i]);
                normalTextures.Add(frame);
            }
            normalAnimation = new Animation(normalTextures, 1, true, "Normal");

            // List with Highlighted animation.
            List<Texture> highlightedTextures = new List<Texture>();
            for (int i = 0; i < highlightPath.Count; i++)
            {
                Texture frame = Engine.GetTexture(highlightPath[i]);
                highlightedTextures.Add(frame);
            }
            highlightedAnimation = new Animation(highlightedTextures, 1, true, "Normal");
        }
    }
}
