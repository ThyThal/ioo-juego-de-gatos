﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class Player : GameObject
    {
        private readonly float originalShootingCooldown = 0.5f;
        private readonly float originalAbilityCooldown = 3f;

        // Change Life Controller.
        public ICollider collider;
        public LifeController lifeController;
        private Animation idleAnimation;
        private float speed;

        private float cooldownReduce = 70;
        private float shootingCooldown;
        private float abilityCooldown;
        private float currentShootingCooldown;
        private float currentAbilityCooldown;
        private bool _aimbot;

        // Power Information.
        public bool pCooldown;
        public float cooldownTimer;
        public bool pDamage;
        public float damageTimer;

        // Bullet Information.
        private float bulletSpeed = 300;
        private int originalDamage;
        private int bulletDamage = 50;
        private Vector2 bulletScale = new Vector2(0.1f,0.1f);
        private Vector2 originalBulletScale;

        // Player Constructor.
        public Player(Vector2 position, Vector2 scale, float angle, float speed, int maxLife) : base (position, scale, angle)
        {
            collider = new BoxCollider(this);
            // Change Life Controller.
            shootingCooldown = originalShootingCooldown;
            abilityCooldown = originalAbilityCooldown;
            originalDamage = bulletDamage;
            originalBulletScale = bulletScale;
            this.lifeController = new LifeController(maxLife);
            this.speed = speed;

            CreateAnimations();
            this.render.currentAnimation = idleAnimation;
        }
        public override void Update()
        {
            currentShootingCooldown -= Program.deltaTime;
            currentAbilityCooldown -= Program.deltaTime;
            if (cooldownTimer > 0) cooldownTimer -= Program.deltaTime;
            if (damageTimer > 0) damageTimer -= Program.deltaTime;

            base.Update();
            CheckPowers();
            InputDetection();
        }

        public void InputDetection()
        {
            // Horizontal Movement
            if (Engine.GetKey(Keys.D) && ((render.transform.Position.X + render.GetOffsetX()) <= 750))
            {
                render.transform.Position = new Vector2(render.transform.Position.X + speed * Program.deltaTime, render.transform.Position.Y);
            }

            if (Engine.GetKey(Keys.A) && ((render.transform.Position.X - render.GetOffsetX()) >= 50))
            {
                render.transform.Position = new Vector2(render.transform.Position.X - speed * Program.deltaTime, render.transform.Position.Y);
            }

            // Shoot Input
            if (Engine.GetKey(Keys.E))
            {
                if (currentShootingCooldown <= 0)
                {
                    ShootBullet();
                }
            }

            // Use Anility
            if (Engine.GetKey(Keys.R))
            {
                if (currentAbilityCooldown <= 0)
                {
                    UseAbility();
                }
            }
        }
        private void CheckPowers()
        {
            if (cooldownTimer > 0)
            {
                shootingCooldown = originalShootingCooldown - ((cooldownReduce / 100) * originalShootingCooldown);
                abilityCooldown = originalAbilityCooldown - ((cooldownReduce / 100) * originalAbilityCooldown);
            } // Shooting Cooldown Reduce.

            else if (cooldownTimer <=0)
            {
                shootingCooldown = originalShootingCooldown;
                abilityCooldown = originalAbilityCooldown;
            } // Shoting Cooldown Original.

            if (damageTimer > 0)
            {
                bulletDamage = originalDamage * 2;
                bulletScale = originalBulletScale * new Vector2(1.5f, 1.5f);
            } // Shoot Bigger Bullets & Stronger.
            else if (damageTimer <= 0)
            {
                bulletDamage = originalDamage;
                bulletScale = originalBulletScale;
            } // Shoot Original Bullets.
        }
        private void ShootBullet()
        {
            Engine.Debug("Shoot");
            currentShootingCooldown = shootingCooldown;
            BulletCreator();
        }
        private void BulletCreator()
        {
            Bullet bullet = LevelManager.Instance.genericBulletPool.GetFromPool();
            bullet.ResetTransform(render.transform.Position, bulletScale, 0);
            bullet.GiveValues(Bullet.Type.Player, bulletSpeed, bulletDamage, _aimbot);
            _aimbot = false;
        }
        private void UseAbility() // Use Aimbot Bullets.
        {
            _aimbot = true;
            ShootBullet();
            currentAbilityCooldown = abilityCooldown;
        }

        protected override void CreateAnimations() // Play Animations.
        {
            List<Texture> idleTextures = new List<Texture>();
            for (int i = 0; i < 3; i++)
            {
                Texture frame = Engine.GetTexture($"Textures/Player/idle/{i}.png");
                idleTextures.Add(frame);
            }
            idleAnimation = new Animation(idleTextures, 0.5f, true, "Idle");
        }
    }
}