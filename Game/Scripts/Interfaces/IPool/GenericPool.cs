﻿using System.Collections.Generic;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class GenericPool<T> where T : IPool<T>
    {
        public List<T> freeObjects;
        public List<T> usedObjects;
        public IPoolGenerator<T> createPoolObject;

        public GenericPool(IPoolGenerator<T> createPool)
        {
            this.createPoolObject = createPool;
            freeObjects = new List<T>();
            usedObjects = new List<T>();
    }

        public T GetFromPool()
        {
            T itemReturn;

            if (freeObjects.Count != 0)
            {
                
                itemReturn = freeObjects[0];
                freeObjects.RemoveAt(0);
            }

            else
            {
                itemReturn = createPoolObject.CreateObject();
                itemReturn.Disabled += Recycle;
            }

            usedObjects.Add(itemReturn);
            itemReturn.ResetObjectValues();
            return itemReturn;
        }

        private void Recycle(T itemRecycle)
        {
            if (usedObjects.Contains(itemRecycle))
            {
                usedObjects.Remove(itemRecycle);
                freeObjects.Add(itemRecycle);
            }
        }
    }
}
