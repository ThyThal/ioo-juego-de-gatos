﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public interface IPool<T>
    {
        void ResetObjectValues();
        event Action<T> Disabled;
    }
}
