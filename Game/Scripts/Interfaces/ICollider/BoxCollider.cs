﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class BoxCollider : ICollider
    {
        public event Action<GameObject> onCollision;
        public GameObject objeto; // get set

        public BoxCollider(GameObject yo)
        {
            objeto = yo;
        }

        public bool IsBoxColliding(GameObject self, GameObject target)
        {
            // Get image dimensions.
            float distanceX = Math.Abs(self.render.transform.Position.X - target.render.transform.Position.X);
            float distanceY = Math.Abs(self.render.transform.Position.Y - target.render.transform.Position.Y);

            // Get center of image.
            float sumHalfWidth = (self.render.Width / 2 + target.render.Width / 2) * self.render.transform.Scale.X;
            float sumHalfHeight = (self.render.Height / 2 + target.render.Height / 2) * self.render.transform.Scale.Y;

            return distanceX <= sumHalfWidth && distanceY <= sumHalfHeight;
        }

        public void CheckCollisions(List<GameObject> target)
        {
            for (int i = target.Count - 1; i >= 0; i--)
            {
                if (IsBoxColliding(objeto, target[i]))
                {
                    onCollision?.Invoke(target[i]);
                }
            }
        }
    }
}
 