﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class QuestionNode : INode
    {
        public delegate bool myDelegate();
        myDelegate question;

        INode trueNode;
        INode falseNode;

        public QuestionNode(myDelegate q, INode tn, INode fn)
        {
            question = q;
            trueNode = tn;
            falseNode = fn;
        }


        public void Execute()
        {
            if (question())
            {
                trueNode.Execute();
            }

            else
            {
                falseNode.Execute();
            }
        }
    }
}
