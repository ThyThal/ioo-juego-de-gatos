﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class ActionNode : INode
    {
        public delegate void myDelegate();
        myDelegate action;

        public ActionNode(myDelegate a)
        {
            action = a;
        }

        public void SubAction(myDelegate newAction)
        {
            action += newAction;
        }

        public void Execute()
        {
            action();
        }
    }
}
