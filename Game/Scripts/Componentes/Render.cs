﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class Render
    {
        public Transform transform;
        public Animation currentAnimation;
        public float Width => currentAnimation.CurrentFrame.Width;
        public float Height => currentAnimation.CurrentFrame.Height;

        public Render()
        {
            this.transform = new Transform();
        }

        public virtual void Renderer()
        {
            Engine.Draw(currentAnimation.CurrentFrame, transform.Position.X, transform.Position.Y, transform.Scale.X, transform.Scale.Y, transform.Angle, GetOffsetX(), GetOffsetY());
        }

        public float GetOffsetX()
        {
            return (Width * transform.Scale.X) / 2f;
        }

        public float GetOffsetY()
        {
            return (Height * transform.Scale.Y) / 2f;
        }         
    }
}
