﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{ 
    public abstract class GameObject
    {
        public Render render;
        public GameObject(Vector2 position, Vector2 scale, float angle)
        {
            render = new Render();
            render.transform.Position = position;
            render.transform.Scale = scale;
            render.transform.Angle = angle;
        }

        public virtual void Update()
        {

        }
        public void Destroy()
        {

        }
        protected abstract void CreateAnimations();

    }
}
