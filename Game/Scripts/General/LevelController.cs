﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class LevelController
    {
        private const string LEVEL_BACKGROUND = "Textures/Level.png";
        public Player player { get; private set; }
        public List<GameObject> playerList;
        public List<GameObject> Bullets { get; private set; } = new List<GameObject>();
        public List<GameObject> Enemies { get; private set; } = new List<GameObject>();

        public GenericPool<Bullet> genericBulletPool;


        private float loseTimer = 120f;
        private float originalTimer;
        public bool isPlaying;

        private static LevelController instance;
        public static LevelController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new LevelController();
                }

                return instance;
            }
        }


        public LevelController()
        {
            Instantiation();
        }

        public void Instantiation()
        {
            originalTimer = loseTimer;
            player = new Player(new Vector2(400, 700), new Vector2(0.1f, 0.1f), 0, 500f, 100, new Render());
            playerList = new List<GameObject>();
            genericBulletPool = new GenericPool<Bullet>(EnemyFactory.Instance);
            playerList.Add(player);
        }

        public void SpawnEnemies(int amount)
        {
            Random random = new Random();
            for (int i = 0; i < amount; i++)
            {
                var posX = random.Next(0, 500);
                var posY = random.Next(0, 500);
                float scale = random.Next(1, 2);
                float scaleX = scale / 10;
                float scaleY = scale / 10;

                int select = random.Next(0, 3);

                if (select == 0)
                {
                    //
                    Enemies.Add(EnemyFactory.Instance.GetInstance((EnemyType.Common), new Vector2(posX, posY), new Vector2(scaleX, scaleY), 0f));
                } // Common Enemy

                else if (select == 1)
                {
                    Enemies.Add(EnemyFactory.Instance.GetInstance((EnemyType.Quick), new Vector2(posX, posY), new Vector2(scaleX, scaleY), 0f));
                } // Quick Enemy

                else if (select == 2)
                {
                    Enemies.Add(EnemyFactory.Instance.GetInstance((EnemyType.Mega), new Vector2(posX, posY), new Vector2(scaleX, scaleY), 0f));
                } // Mega Enemy
            }            
        }

        public void StartGame()
        {
            isPlaying = true;
            loseTimer = originalTimer;
            player.lifeController.GetHeal(100);
            SpawnEnemies(7);
        }

        public void Update()
        {
            loseTimer -= Program.deltaTime;

            UpdatePlayer();
            UpdateBullets();
            UpdateEnemy();

            if (isPlaying)
            {
                if (loseTimer <= 0 || player.lifeController.IsAlive == false)
                {
                    GameOver();
                } // Lose Condition
                if ( Enemies.Count == 0 && loseTimer > 0 ) 
                {
                    YouWin();
                } // Win Condition
            }
        }

        public void Render()
        {
            Engine.Draw(LEVEL_BACKGROUND, 0, 0);
            if (player != null)
            {
                player.render.Renderer();
            } // Player Renderer            
            for (int i = Bullets.Count - 1; i >= 0; i--)
            {
                Bullets[i].render.Renderer();
            } // Bullets Renderer
            for (int i = Enemies.Count - 1; i >= 0; i--)
            {
                Enemies[i].render.Renderer();
            } // Enemies Renderer
        }

        // Update Methods.
        private void UpdatePlayer()
        {
            if (player != null)
            {
                player.InputDetection();
                player.Update();
            } // Player Update
        }
        private void UpdateBullets()
        {
            for (int i = Bullets.Count - 1; i >= 0; i--)
            {
                Bullets[i].Update();
            } // Bullets Update
        }
        private void UpdateEnemy()
        {
            for (int i = Enemies.Count - 1; i >= 0; i--)
            {
                Enemies[i].Update();
            } // Enemies Update
        }

        // Game States
        public void GameOver()
        {
            GameManager.Instance.ChangeGameState(GameState.GameOverScreen);
            isPlaying = false;
        }
        public void YouWin()
        {
            GameManager.Instance.ChangeGameState(GameState.WinScreen);
            isPlaying = false;
        }
    }
}
