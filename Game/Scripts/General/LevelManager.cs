﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class LevelManager
    {
        private const string LEVEL_BACKGROUND = "Textures/Level.png";
        public Player player { get; private set; }
        public List<GameObject> playerList;
        public List<GameObject> Bullets { get; private set; } = new List<GameObject>();
        public List<GameObject> Enemies { get; private set; } = new List<GameObject>();
        public List<GameObject> Powers { get; private set; } = new List<GameObject>();

        public GenericPool<Bullet> genericBulletPool;
        public GenericPool<PowerUp> genericPowerPool;

        private float loseTimer = 120f;
        private float spawnTimer = 1.75f;
        private float checkWaveTimer = 3f;
        private float powerTimer = 7f;

        private float originalPowerTimer;
        private float originalTimer;
        private float originalSpawnTimer;
        private float originalCheckWave;

        private bool spawning;
        private int currentWave;
        private int enemiesAmount;
        private int currentSpawned;
        public bool isPlaying;

        private static LevelManager instance;

        public static LevelManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new LevelManager();
                }

                return instance;
            }
        }

        public LevelManager()
        {
            Instantiation();
        }
        public void Instantiation()
        {
            originalSpawnTimer = spawnTimer;
            originalTimer = loseTimer;
            originalPowerTimer = powerTimer;

            player = new Player(new Vector2(400, 700), new Vector2(0.1f, 0.1f), 0, 500f, 100);
            playerList = new List<GameObject>();
            genericBulletPool = new GenericPool<Bullet>(EnemyFactory.Instance);
            genericPowerPool = new GenericPool<PowerUp>(PowerFactory.Instance);
            playerList.Add(player);
        }
        public void StartGame()
        {
            isPlaying = true;
            currentWave = 0;
            loseTimer = originalTimer;
            powerTimer = originalPowerTimer;
            spawnTimer = 0;
            originalCheckWave = checkWaveTimer;
            player.lifeController.GetHeal(100);
            player.cooldownTimer = 0;
            player.damageTimer = 0;
            SpawnNextWave();
        }

        public void Update()
        {
            spawnTimer -= Program.deltaTime;
            loseTimer -= Program.deltaTime;
            checkWaveTimer -= Program.deltaTime;
            powerTimer -= Program.deltaTime;

            if (!spawning) 
            {
                CheckWave();
            } // Check Wave Amount if spawning finished.
            if (spawning)
            {
                spawnTimer -= Program.deltaTime;
                if (currentSpawned < enemiesAmount)
                {
                    if (spawnTimer <= 0)
                    {
                        SpawnEnemy();
                    }
                }
                else
                {
                    spawning = false;
                }
            } // If Spawning Enemies can't win/lose.
            if (powerTimer <= 0) SpawnPower();

            UpdatePlayer();
            UpdateBullets();
            UpdateEnemys();
            UpdatePowers();

            if (isPlaying)
            {
                if (loseTimer <= 0 || player.lifeController.IsAlive == false)
                {
                    GameOver();
                } // Lose Condition
                if ((Enemies.Count == 0) && (loseTimer > 0) && !spawning && (currentWave >= 3))
                {
                    YouWin();
                } // Win Condition
            } // Win Condition Check.
        }

        // Wave Methods.
        public void SpawnEnemy()
        {
            Random random = new Random();
            var posX = random.Next(100, 700);
            var posY = random.Next(100, 500);
            float scale = random.Next(1, 2);
            float scaleX = scale / 10;
            float scaleY = scale / 10;

            int select = random.Next(0, 3);
            Enemies.Add(EnemyFactory.Instance.GetInstance((EnemyType)select, new Vector2(posX, posY), new Vector2(scaleX, scaleY), 0f));
            currentSpawned = currentSpawned + 1;
            spawnTimer = originalSpawnTimer;
        }
        private void SpawnPower()
        {
            Random random = new Random();
            int powerPosition = random.Next(50, 750);
            int powerSelect = random.Next(0, 2);

            PowerUp power = LevelManager.Instance.genericPowerPool.GetFromPool();
            power.ResetTransform(new Vector2(powerPosition, -50), new Vector2(0.25f, 0.25f), 0f);
            power.GiveValues((PowerUp.Type)(PowerType)powerSelect, 120f, playerList);
            powerTimer = originalPowerTimer;
        }
        private void CheckWave()
        {
            if (checkWaveTimer <= 0)
            {
                if (Enemies.Count <= 0)
                {
                    SpawnNextWave();
                    checkWaveTimer = originalCheckWave;
                }
            }
        }
        private void SpawnNextWave()
        {
            Engine.Debug($"Wave {currentWave}");
            spawning = true;
            currentSpawned = 0;

            if (currentWave == 0)
            {
                enemiesAmount = 3;
            }

            if (currentWave == 1)
            {
                enemiesAmount = 5;
            }

            if (currentWave == 2)
            {
                enemiesAmount = 9;
            }

            if (currentWave > 2)
            {
                return;
            }

            currentWave = currentWave + 1;
        }

        public void Render()
        {
            Engine.Draw(LEVEL_BACKGROUND, 0, 0);
            if (player != null)
            {
                player.render.Renderer();
            } // Player Renderer            
            for (int i = Bullets.Count - 1; i >= 0; i--)
            {
                Bullets[i].render.Renderer();
            } // Bullets Renderer
            for (int i = Enemies.Count - 1; i >= 0; i--)
            {
                Enemies[i].render.Renderer();
            } // Enemies Renderer
            for (int i = Powers.Count - 1; i >= 0; i--)
            {
                Powers[i].render.Renderer();
            }
        }

        // Update Methods.
        private void UpdatePlayer()
        {
            if (player != null)
            {
                player.InputDetection();
                player.Update();
            } // Player Update
        }
        private void UpdateBullets()
        {
            for (int i = Bullets.Count - 1; i >= 0; i--)
            {
                Bullets[i].Update();
            } // Bullets Update
        }
        private void UpdateEnemys()
        {
            for (int i = Enemies.Count - 1; i >= 0; i--)
            {
                Enemies[i].Update();
            } // Enemies Update
        }
        private void UpdatePowers()
        {
            for (int i = Powers.Count - 1; i >= 0; i--)
            {
                Powers[i].Update();
            } // Powers Update
        }

        // Game States
        public void GameOver()
        {
            Engine.Debug("\nYou Lose!");
            GameManager.Instance.ChangeGameState(GameState.GameOverScreen);
            isPlaying = false;
        }
        public void YouWin()
        {
            Engine.Debug("\nYou Win!");
            GameManager.Instance.ChangeGameState(GameState.WinScreen);
            isPlaying = false;
        }
    }
}
