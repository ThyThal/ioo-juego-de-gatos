﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public enum GameState
    {
        MainMenu,
        Credits,
        GameOverScreen,
        WinScreen,
        Level
    }

    public class GameManager
    {
        public const string GAME_OVER_TEXTURE = "Textures/Menus/GameOver.png";
        public const string WIN_TEXTURE = "Textures/Menus/YouWin.png";
        public const string CREDITS_TEXTURE = "Textures/Menus/Credits.png";


        public LevelManager LevelController { get; private set; }
        public MainMenu MainMenu { get; private set; }
        public CreditsScreen Credits { get; private set; }
        public FinishScreen WinScreen { get; private set; }
        public FinishScreen GameOverScreen { get; private set; }

        private static GameManager instance;
        public static GameManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new GameManager();
                }

                return instance;
            }
        }

        public GameState CurrentState { get; private set; }

        public void Initialization()
        {
            ChangeGameState(GameState.MainMenu);
            MainMenu = new MainMenu();
            WinScreen = new FinishScreen(WIN_TEXTURE);
            GameOverScreen = new FinishScreen(GAME_OVER_TEXTURE);
            Credits = new CreditsScreen(CREDITS_TEXTURE);
            LevelController = new LevelManager();

        }

        public void Update()
        {
            switch (CurrentState)
            {
                case GameState.MainMenu:
                    LevelController.isPlaying = false;
                    LevelController.Enemies.Clear();
                    LevelController.Bullets.Clear();
                    LevelController.Powers.Clear();
                    MainMenu.Update();
                    break;

                case GameState.Credits:
                    Credits.Update();
                    break;

                case GameState.GameOverScreen:
                    GameOverScreen.Update();
                    break;

                case GameState.WinScreen:
                    WinScreen.Update();
                    break;

                case GameState.Level:
                    if (LevelController.isPlaying == false)
                    {
                        LevelController.StartGame();
                    }
                    LevelController.Update();
                    break;

                default:
                    break;
            }
        }

        public void Render()
        {
            if (Engine.GetKey(Keys.F1))
            {
                ChangeGameState(GameState.WinScreen);
            }

            if (Engine.GetKey(Keys.F2))
            {
                ChangeGameState(GameState.GameOverScreen);
            }

            switch (CurrentState)
            {
                case GameState.MainMenu:
                    MainMenu.Renderer();
                    break;

                case GameState.Credits:
                    Credits.Render();
                    break;

                case GameState.GameOverScreen:
                    GameOverScreen.Render();
                    break;

                case GameState.WinScreen:
                    WinScreen.Render();
                    break;

                case GameState.Level:
                    LevelController.Render();
                    break;

                default:
                    MainMenu.Renderer();
                    break;
            }
        }

        public void ChangeGameState(GameState newState)
        {
            CurrentState = newState;
        }

        public void ExitGame()
        {
            Environment.Exit(1);
        }
    }
}
