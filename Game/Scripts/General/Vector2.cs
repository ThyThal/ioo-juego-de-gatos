﻿using System;
using System.Collections.Generic;


namespace Game
{
    public struct Vector2
    {
        private float x;
        private float y;

        public float X { get => x; set => x = value; }
        public float Y { get => y; set => y = value; }

        // Constructors.
        public Vector2(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        // Static Properties.
        public static Vector2 Down => new Vector2(0, -1);
        public static Vector2 Left => new Vector2(-1, 0);
        public static Vector2 One => new Vector2(1, 1);
        public static Vector2 Right => new Vector2(1, 0);
        public static Vector2 Up => new Vector2(0, 1);
        public static Vector2 Zero => new Vector2(0, 0);

        // Properties.
        public static float Magnitude(Vector2 vector)
        {
            return (float) Math.Sqrt((vector.x * vector.x) + (vector.y * vector.y));
        }

        public static Vector2 Normalize(Vector2 vector)
        {
            float mag = Magnitude(vector);
            Vector2 norm = vector / mag;
            return norm;
        }


        // Operators.
        public static Vector2 operator +(Vector2 vector1, Vector2 vector2)
        {
            return new Vector2(vector1.x + vector2.x, vector1.y + vector2.y);
        }

        public static Vector2 operator -(Vector2 vector1, Vector2 vector2)
        {
            return new Vector2(vector1.x +- vector2.x, vector1.y - vector2.y);
        }

        public static bool operator ==(Vector2 vector1, Vector2 vector2)
        {
            return vector1.x == vector2.x && vector1.y == vector2.y;
        }

        public static bool operator !=(Vector2 vector1, Vector2 vector2)
        {
            return vector1.x != vector2.x || vector1.y != vector2.y;
        }

        public static Vector2 operator /(Vector2 vector1, Vector2 vector2)
        {
            return new Vector2(vector1.x / vector2.x, vector1.y / vector2.y);
        }

        public static Vector2 operator /(Vector2 vector1, float float1)
        {
            return new Vector2(vector1.x / float1, vector1.y / float1);
        }

        public static Vector2 operator *(Vector2 vector1, Vector2 vector2)
        {
            return new Vector2(vector1.x * vector2.x, vector1.y * vector2.y);
        }

        public static Vector2 operator *(Vector2 vector1, float float1)
        {
            return new Vector2(vector1.x * float1, vector1.y * float1);
        }
    }
}
