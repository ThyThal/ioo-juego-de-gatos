﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class Bullet : GameObject, ICollider, IPool<Bullet>
    {
        public enum Type { Player, Enemy };
        private Type type;

        public List<GameObject> listBullets;
        public List<GameObject> playerList;
        public List<GameObject> enemyList;

        public BoxCollider collider;
        private Animation idleAnimation;
        private List<GameObject> _target;
        private Bird closestTarget;
        private Vector2 distance;

        private float speed;
        private float lifeTime = 8f;
        private float currentLifeTime;
        private int damage;
        private bool _aimbot;
        public int Damage => damage;

        public Bullet(Vector2 position, Vector2 scale, float angle, List<GameObject> listBullets, List<GameObject> playerList, List<GameObject> enemyList) : base(position, scale, angle)
        {
            this.listBullets = listBullets;
            this.playerList = playerList;
            this.enemyList = enemyList;
        }

        public void Start()
        {

        }

        public override void Update()
        {
            base.Update();
            currentLifeTime += Program.deltaTime;

            Engine.Debug("Bullet Update");
            if (currentLifeTime >= lifeTime)
            {
                DestroyBullet();
            }


                
            DoMovement();


            collider.CheckCollisions(_target); // Collision de Bala contra Objetivo.

        }

        private void DoMovement()
        {
            if (_aimbot)
            {
                if (closestTarget.lifeController.IsAlive == false)
                {
                    SelectAimbotTarget(_target);
                    return;
                }

                Vector2 predictVector = (closestTarget.render.transform.Position - render.transform.Position);
                var predict = Vector2.Normalize(predictVector);
                render.transform.Position += predict * speed * Program.deltaTime;
            }

            else
            {
                render.transform.Position = new Vector2(render.transform.Position.X, render.transform.Position.Y - speed * Program.deltaTime);
            }

            render.currentAnimation.Update();
        }

        private void SelectAimbotTarget(List<GameObject> list)
        {
            if (list.Count == 0)
                return;

            closestTarget = list[0] as Bird;
        }

        // Crear y seleccionar las animaciones.
        protected override void CreateAnimations()
        {
            SelectType(this.type);
        }
        private void SelectType(Type t)
        {
            switch (t)
            {
                case Type.Player:
                    TypePlayer();
                    _target = enemyList;
                    break;

                case Type.Enemy:
                    TypeEnemy();
                    _target = playerList;
                    break;

                default:
                    break;
            }
        }
        private void TypePlayer()
        {
            List<Texture> idleTextures = new List<Texture>();
            for (int i = 0; i < 1; i++)
            {
                Texture frame = Engine.GetTexture($"Textures/Bullet/player/{i}.png");
                idleTextures.Add(frame);
            }
            idleAnimation = new Animation(idleTextures, 0.1f, true, "Idle");
        }
        private void TypeEnemy()
        {
            speed = -speed;
            List<Texture> idleTextures = new List<Texture>();
            for (int i = 0; i < 1; i++)
            {
                Texture frame = Engine.GetTexture($"Textures/Bullet/enemy/{i}.png");
                idleTextures.Add(frame);
            }
            idleAnimation = new Animation(idleTextures, 0.1f, true, "Idle");
        }

        // Collisiones y Destruir Bala.
        public void DestroyBullet()
        {
            RemoveBullet(listBullets);
            Disabled?.Invoke(this);
        }
        public void CheckCollisions(List<GameObject> target)
        {

        }

        private void ColliderHandler(GameObject g)
        {
            Bird b = g as Bird;
            if (b != null)
            {
                b.lifeController.GetDamage(Damage);
                DestroyBullet();
                return;
            }

            Player p = g as Player;
            if (p != null)
            {
                p.lifeController.GetDamage(Damage);
                DestroyBullet();
            }
        }


        // Pool Methods
        public event Action<Bullet> Disabled;
        public void ResetObjectValues()
        {
            currentLifeTime = 0;
        }
        public void ResetTransform(Vector2 position, Vector2 scale, float angle)
        {
            this.render.transform.Position = position;
            this.render.transform.Scale = scale;
            this.render.transform.Angle = angle;
        }
        public void GiveValues(Type type, float speed, int damage, bool aimbot)
        {
            // Type Selection
            this.type = type;
            _aimbot = aimbot;
            collider = new BoxCollider(this);
            this.speed = speed;
            this.damage = damage;
            AddBullet(listBullets);
            CreateAnimations();
            SelectAimbotTarget(_target);
            this.render.currentAnimation = idleAnimation;
            collider.onCollision += ColliderHandler; // Bullet onCollision.
        }
        public void AddBullet(List<GameObject> listBullets)
        {
            listBullets.Add(this);
        }
        public void RemoveBullet(List<GameObject> listBullets)
        {
            listBullets.Remove(this);
        }
    }
}