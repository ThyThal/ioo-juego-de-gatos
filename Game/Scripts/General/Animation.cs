﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class Animation
    {
        private List<Texture> frames;
        private int currentFramesIndex = 0;
        public float speed;
        private float currentAnimationTime;
        private bool isLoopEnabled;
        private string name;

        public int FramesCount => frames.Count;
        public int CurrentFrameIndex => currentFramesIndex;

        public Texture CurrentFrame => frames[currentFramesIndex];

     
        public Animation(List<Texture> textureList, float speed, bool isLoopEnabled = true, string name ="Default")
        {
            if (textureList != null)
            {
                this.frames = textureList;
            }
            
            this.speed = speed;
            this.isLoopEnabled = isLoopEnabled;
            this.name = name;
            this.speed = speed;
        }

        public void Update()
        {
            currentAnimationTime += Program.deltaTime;

            if (currentAnimationTime >= speed)
            {
                currentFramesIndex++;
                currentAnimationTime = 0;

                if (currentFramesIndex >= frames.Count)
                {
                    if (isLoopEnabled)
                    {
                        currentFramesIndex = 0;
                    }
                    else
                    {
                        currentFramesIndex = frames.Count - 1;
                    }
                }
            }
        }
    }
}
