﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class PowerUp : GameObject, ICollider, IPool<PowerUp>
    {
        public enum Type { Damage = 0, Cooldown = 1 };
        private Type type;

        private BoxCollider collider;
        public List<GameObject> target;
        private Animation idleAnimation;
        public event Action<PowerUp> Disabled;

        private float speed;
        private float currentLifetime;
        private float lifetime = 10f;
        private float powerTimer;

        public PowerUp(Vector2 position, Vector2 scale, float angle) : base(position, scale, angle)
        {
            
        }

        public override void Update()
        {
            render.transform.Position = new Vector2(render.transform.Position.X, render.transform.Position.Y + speed * Program.deltaTime);
            collider.CheckCollisions(target); // Collision de Power contra Player.
        }

        public void CheckCollisions(List<GameObject> target)
        {

        }

        // Asignacion de valores.
        public void GiveValues(Type type, float speed, List<GameObject> target)
        {
            this.type = type;
            this.speed = speed;
            this.target = target;

            collider = new BoxCollider(this);
            GameManager.Instance.LevelController.Powers.Add(this);
            CreateAnimations();
            render.currentAnimation = idleAnimation;
            collider.onCollision += ColliderHandler; // Power onCollision.
        }
        public void ResetTransform(Vector2 position, Vector2 scale, float angle)
        {
            this.render.transform.Position = position;
            this.render.transform.Scale = scale;
            this.render.transform.Angle = angle;
        }
        public void ResetObjectValues()
        {
            currentLifetime = lifetime;
            Random random = new Random();
            powerTimer = random.Next(5,8);
        }

        // Animations.
        protected override void CreateAnimations()
        {
            SelectType(type);
        } // Create Animations.
        private void SelectType(Type t)
        {
            switch (t)
            {
                case Type.Damage:
                    TypeDamage();
                    break;

                case Type.Cooldown:
                    TypeCooldown();
                    break;

                default:
                    break;
            }
        } // Switch.
        private void TypeDamage()
        {
            List<Texture> idleTextures = new List<Texture>();
            for (int i = 0; i < 1; i++)
            {
                Texture frame = Engine.GetTexture($"Textures/Power/Damage/{i}.png");
                idleTextures.Add(frame);
            }
            idleAnimation = new Animation(idleTextures, 0.1f, true, "Idle");
        } // Type Damage.
        private void TypeCooldown()
        {
            List<Texture> idleTextures = new List<Texture>();
            for (int i = 0; i < 1; i++)
            {
                Texture frame = Engine.GetTexture($"Textures/Power/Cooldown/{i}.png");
                idleTextures.Add(frame);
            }
            idleAnimation = new Animation(idleTextures, 0.1f, true, "Idle");
        } // Type Cooldown.

        // Collision.
        private void ColliderHandler(GameObject g)
        {
            Player p = g as Player;
            if (p != null)
            {
                if (type == Type.Cooldown)
                {
                    p.cooldownTimer += powerTimer;
                    p.pCooldown = true;
                }

                if (type == Type.Damage)
                {
                    p.damageTimer += powerTimer;
                    p.pDamage = true;
                }

                DestroyPower();
                return;
            }

        }
        public void DestroyPower()
        {
            GameManager.Instance.LevelController.Powers.Remove(this);
            Disabled?.Invoke(this);
        }
    }
}

