﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public enum EnemyType
    {
        Common = 0,
        Quick = 1,
        Mega = 2
    }

    public class EnemyFactory : IPoolGenerator<Bullet>
    {
        private static EnemyFactory instance;
        public static EnemyFactory Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new EnemyFactory();
                }
                return instance;
            }
        }

        float animSpeed, moveSpeed, shootSpeed;

        public Bird GetInstance(EnemyType enemyType, Vector2 position, Vector2 scale, float angle)
        {
            Randomize();

            switch (enemyType)
            {
                case EnemyType.Common:                    
                    return new Common(position, scale, angle, animSpeed, moveSpeed, 100, shootSpeed);

                case EnemyType.Quick:
                    return new Quick(position, scale, angle, (animSpeed * 0.5f), (moveSpeed * 2f), 100, shootSpeed -1f);

                case EnemyType.Mega:
                    return new Mega(position, (scale * new Vector2(2, 2)), angle, (animSpeed * 2f), (moveSpeed * 0.75f), 100, shootSpeed + 2f);

                default:
                    return new Common(position, scale, angle, animSpeed, moveSpeed, 100, shootSpeed);
            }
        }

        private void Randomize()
        {
            Random random = new Random();
            animSpeed = (float)random.Next(10, 25) / 100; // Animation Speed
            moveSpeed = random.Next(100, 200);
            shootSpeed = random.Next(3, 5);
        }

        public Bullet CreateObject()
        {
            Bullet bullet = new Bullet(Vector2.Zero, Vector2.Zero, 0, GameManager.Instance.LevelController.Bullets, GameManager.Instance.LevelController.playerList, GameManager.Instance.LevelController.Enemies);
            return bullet;
        }
    }
}
