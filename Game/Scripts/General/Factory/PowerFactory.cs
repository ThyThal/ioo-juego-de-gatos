﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public enum PowerType
    {
        Damage = 0,
        Cooldown = 1
    }

    class PowerFactory : IPoolGenerator<PowerUp>
    {
        private static PowerFactory instance;
        public static PowerFactory Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PowerFactory();
                }
                return instance;
            }
        }
        public PowerUp CreateObject()
        {
            PowerUp power = new PowerUp(Vector2.Zero, Vector2.Zero, 0f);
            return power;
        }

        Vector2 position = Vector2.Zero;
        Vector2 scale = Vector2.One;
        float angle = 0f;

        public PowerUp GetInstance(PowerType powerType, Vector2 position, Vector2 scale, float angle)
        {
            Randomize();

            switch (powerType)
            {
                case PowerType.Damage:
                    return new PowerUp(position, scale, angle);

                case PowerType.Cooldown:
                    return new PowerUp(position, scale, angle);

                default:
                    return new PowerUp(position, scale, angle);
            }
        }
        private void Randomize()
        {
            Random random = new Random();
            position = new Vector2((float)random.Next(100, 700), -50f);
        }

    }
}
