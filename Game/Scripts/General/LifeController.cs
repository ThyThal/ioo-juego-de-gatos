﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class LifeController
    {
        private int currentLife;
        private int maxLife;

        public event Action OnDead;
        public event Action<int, int> OnGetDamage; // Parametro 1 = CurrentLife ---- Parametro 2 = DamageAmount
        public event Action<int, int> OnGetHeal;  // Parametro 1 = CurrentLife ---- Parametro 2 = HealAmount

        public bool IsAlive => currentLife > 0;

        public int CurrentLife
        {
            get => currentLife;

            set
            {
                currentLife = value;

                if (!IsAlive)
                {
                    OnDead?.Invoke();
                }

                if (currentLife > maxLife)
                {
                    currentLife = maxLife;
                }
            }
        }

        public LifeController(int maxLife)
        {
            this.maxLife = maxLife;
            CurrentLife = maxLife;
        }

        public void GetDamage(int damage)
        {
            CurrentLife -= damage;
            OnGetDamage?.Invoke(CurrentLife, damage);
        }

        public void GetHeal(int heal)
        {
            CurrentLife += heal;
            OnGetHeal?.Invoke(CurrentLife, heal);
        }
    }
}
