﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public abstract class Bird : GameObject
    {
        public INode init;
        public ICollider collider;
        public LifeController lifeController;

        private float currentShootingCooldown;
        private float shootingCooldown;
        private float limitRigth =750;
        private float limitLeft = 50;
        private float speed;

        // Constructor
        public Bird (Vector2 position, Vector2 scale, float angle, float animSpeed, float moveSpeed, int maxLife, float shootSpeed) : base(position, scale, angle)
        {
            collider = new BoxCollider(this);
            this.lifeController = new LifeController(maxLife);
            speed = moveSpeed;

            Random random = new Random();
            int dir = random.Next(0, 2);
            if (dir == 1)
            {
                speed = -speed;
            }

            lifeController.OnGetDamage += OnGetDamageHandler;
            lifeController.OnGetHeal += OnGetHealHandler;
            lifeController.OnDead += OnDeadHandler;

            shootingCooldown = shootSpeed;
            currentShootingCooldown = shootingCooldown;
            Decisiones();
            
        }

        // Update
        public override void Update()
        {
            currentShootingCooldown -= Program.deltaTime;
            render.currentAnimation.Update();
            init.Execute();
        }

        // Inteligencia Artificial
        public virtual void Decisiones()
        {
            ActionNode a_Shoot = new ActionNode(ShootBullet);
            ActionNode a_Move = new ActionNode(Move);
            ActionNode a_Die = new ActionNode(Die);

            QuestionNode q_Cooldown = new QuestionNode(ShootingCooldown, a_Shoot, a_Move);
            QuestionNode q_Alive = new QuestionNode(IsAlive, q_Cooldown, a_Die);

            init = q_Alive;
        }

        //      === Inteligencia Artificial // Acciones y Preguntas ===
        #region === Inteligencia Artificial // Acciones y Preguntas ===
        public virtual void Move() // MOVERSE
        {
            if (render.transform.Position.X >= limitRigth || render.transform.Position.X <= limitLeft)
            {
                speed = -speed;
            } // Change speeed on border of screen.

            render.transform.Position = new Vector2(render.transform.Position.X + speed * Program.deltaTime, render.transform.Position.Y);
        }
        public virtual void ShootBullet() // DISPARAR
        {
            currentShootingCooldown = shootingCooldown;
        }
        public virtual void Die() // MORIR
        {
            GameManager.Instance.LevelController.Enemies.Remove(this);
        }
        public virtual bool ShootingCooldown()
        {
            if (currentShootingCooldown <= 0)
            {
                return true;
            }

            else
            {
                return false;
            }
        }
        public virtual bool IsAlive()
        {
            if (lifeController.IsAlive)
            {
                return true;
            }

            else
            {
                return false;
            }
        }
        #endregion

        //      === Handlers ===
        #region === Handlers ===
        private void OnGetDamageHandler(int currentLife, int damageAmount)
        {
            //Mostrar animacion de que me hicieron daño
        }
        private void OnGetHealHandler(int currenLife, int healAmount)
        {
            //Mostrar animacion de que me cure
        }
        private void OnDeadHandler()
        {
            //Animacion de muerte
            /*lifeController.OnDead -= OnDeadHandler;
            lifeController.OnGetDamage -= OnGetDamageHandler;
            lifeController.OnGetHeal -= OnGetHealHandler;*/
        }
        #endregion
    }
}
