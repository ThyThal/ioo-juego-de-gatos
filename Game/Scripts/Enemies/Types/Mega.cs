﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class Mega : Bird
    {
        private Animation idleAnimation;
        private float animSpeed;


        // Bullet Information.
        private float bulletSpeed = 100;
        private float bulletAngle = 0;
        private int bulletDamage = 100;
        private float scaX = 0.15f;
        private float scaY = 0.15f;

        public Mega(Vector2 position, Vector2 scale, float angle, float animSpeed, float moveSpeed, int maxLife, float shootSpeed) : base(position, scale, angle, animSpeed, moveSpeed, maxLife, shootSpeed)
        {
            this.animSpeed = animSpeed;

            CreateAnimations();
            this.render.currentAnimation = idleAnimation;
        }
        public override void Update()
        {
            base.Update();
            if (Engine.GetKey(Keys.H))
            {
                Die();
            }
        }
        protected override void CreateAnimations()
        {
            List<Texture> idleTexture = new List<Texture>();
            for (int i = 0; i < 3; i++)
            {
                Texture frame = Engine.GetTexture($"Textures/Enemy/Mega/{i}.png");
                idleTexture.Add(frame);
            }
            idleAnimation = new Animation(idleTexture, animSpeed, true, "Idle");
        } // Crear animaciones.
        public override void ShootBullet() // DISPARAR
        {
            base.ShootBullet();

            Bullet bullet = LevelManager.Instance.genericBulletPool.GetFromPool();
            bullet.ResetTransform(render.transform.Position, new Vector2(scaX, scaY), 0);
            bullet.GiveValues(Bullet.Type.Enemy, bulletSpeed, bulletDamage, false);
        }

        public override void Die()
        {
            Random random = new Random();
            var sons = random.Next(2, 5);
            var count = sons;
            for (int i = 0; i < sons; i++)
            {

                float scale = random.Next(1, 2);
                float scaleX = scale / 10;
                float scaleY = scale / 10;
                GameManager.Instance.LevelController.Enemies.Add(EnemyFactory.Instance.GetInstance((EnemyType.Quick), this.render.transform.Position, new Vector2(scaleX, scaleY), 0f));
                count -= 1;
            }
            GameManager.Instance.LevelController.Enemies.Remove(this);
        }
    }
}
