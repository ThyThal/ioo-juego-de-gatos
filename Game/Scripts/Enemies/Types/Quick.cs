﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class Quick : Bird
    {
        private Animation idleAnimation;
        private float animSpeed;

        // Bullet Information.
        private float bulletSpeed = 200;
        private float bulletAngle = 180;
        private int bulletDamage = 40;
        float scaX = 0.075f;
        float scaY = 0.075f;

        public Quick(Vector2 position, Vector2 scale, float angle, float animSpeed, float moveSpeed, int maxLife, float shootSpeed) : base(position, scale, angle, animSpeed, moveSpeed, maxLife, shootSpeed)
        {
            this.animSpeed = animSpeed * 1.5f;
            CreateAnimations();
            this.render.currentAnimation = idleAnimation;
        }

        public override void Update()
        {
            base.Update();
        }
        protected override void CreateAnimations()
        {
            List<Texture> idleTexture = new List<Texture>();
            for (int i = 0; i < 3; i++)
            {
                Texture frame = Engine.GetTexture($"Textures/Enemy/Quick/{i}.png");
                idleTexture.Add(frame);
            }
            idleAnimation = new Animation(idleTexture, animSpeed, true, "Idle");
        } // Crear animaciones.

        public override void ShootBullet() // DISPARAR
        {
            base.ShootBullet();

            Bullet bullet = LevelManager.Instance.genericBulletPool.GetFromPool();
            bullet.ResetTransform(render.transform.Position, new Vector2(scaX, scaY), 0);
            bullet.GiveValues(Bullet.Type.Enemy, bulletSpeed, bulletDamage, false);
        }
    }
}
