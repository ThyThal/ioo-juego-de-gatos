﻿using System;
using System.Collections.Generic;
using System.Media;

namespace Game
{
    public class Program
    {
        public static float deltaTime;
        private static float lastFrameTime;
        private static DateTime startDate;

        private static void Main(string[] args)
        {
            Initialization();
           
            while (true)
            {
                var currentTime = (float)(DateTime.Now - startDate).TotalSeconds;
                deltaTime = currentTime - lastFrameTime;
                lastFrameTime = currentTime;
                Update();
                Render();
            }
        }

        private static void Initialization()
        {
            Engine.Initialize("Juego de Gatos!", 800, 800);
            GameManager.Instance.Initialization();
            startDate = DateTime.Now;
            //InitAudio();   
        }

        private static void InitAudio()
        {
            SoundPlayer music = new SoundPlayer("Music/Song.wav");
            music.PlayLooping();
        }

        private static void Update()
        {
            GameManager.Instance.Update();
        }

        private static void Render()
        {
            Engine.Clear();
            GameManager.Instance.Render();
            Engine.Show();        
        }
    }
}